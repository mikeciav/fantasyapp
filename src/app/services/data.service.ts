import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  url = 'https://newsapi.org/v2';
  apiKey = 'a8d080a36630493783eba30609e709c4';
  totalPosts = null;
  pages: any;

  tournament: any;

  tournaments = [
    {
      order: '1',
      name: 'Huntington Beach Open',
      location: 'Huntington Beach',
      date: 'May 3-5 2019',
      m_bvbInfoId: '3669',
      w_bvbInfoId: '3670',
      m_competitionId: '27',
      w_competitionId: '28',
      isOpen: true
    },
    {
      order: '2',
      name: 'Austin Open',
      location: 'Austin Texas',
      date: 'May 17-19 2019',
      m_bvbInfoId: '3671',
      w_bvbInfoId: '3672',
      m_competitionId: '29',
      w_competitionId: '30',
      isOpen: false
    },
    {
      order: '3',
      name: 'Gold Series NYC Open, New York',
      location: 'Huntington Beach',
      date: 'Jun 7-9 2019',
      m_bvbInfoId: '3673',
      w_bvbInfoId: '3674',
      m_competitionId: '31',
      w_competitionId: '32',
      isOpen: false
    }
  ];

  teamAthletes = [];


  constructor(private http: HttpClient) {}

getTeamAthletes(){
  return true;
}


  getTournaments() {
    return this.tournaments;
  }
  getLiveTournament() {
    this.tournament = {
      order: '1',
      name: 'Huntington Beach Open',
      location: 'Huntington Beach',
      date: 'May 3-5 2019',
      m_bvbInfoId: '3669',
      w_bvbInfoId: '3670',
      m_competitionId: '27',
      w_competitionId: '28',
      isOpen: true
    };
    return this.tournament;
  }

  getTopNews(cat) {
    return this.http
      .get(
        `${this.url}/top-headlines?category=${cat}&country=us&apiKey=${this.apiKey}`
        // tslint:disable-next-line:no-string-literal
      )
      .pipe(map(res => res['articles']));
  }
  getSportSources() {
    return (
      this.http
        .get(`${this.url}/sources?category=sports&language=en&apiKey=${this.apiKey}`)
        // tslint:disable-next-line:no-string-literal
        .pipe(map(res => res['sources']))
    );
  }
  getRandomUser() {
    return (
      this.http
        .get(`https://randomuser.me/api?results=20`)
        // tslint:disable-next-line:no-string-literal
        .pipe(map(res => res['results']))
    );
  }
}
