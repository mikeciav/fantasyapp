import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ErrorModalComponent} from './modals/error-modal/error-modal.component';
import {SuccessModalComponent} from './modals/success-modal/success-modal.component';
import {IonicModule} from '@ionic/angular';

@NgModule({
    imports: [
        CommonModule,
        IonicModule.forRoot(),
    ],
    declarations: [ ErrorModalComponent, SuccessModalComponent
    ],
    exports: [ErrorModalComponent, SuccessModalComponent]
})
export class SharedModule { }