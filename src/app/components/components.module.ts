import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { PlayerComponent } from './player/player.component';
import { PlayerMatchStatsComponent } from './player-match-stats/player-match-stats.component';
import {PipesModule} from '../pipes/pipes.module';

@NgModule({
  declarations: [PlayerComponent, PlayerMatchStatsComponent],
  imports: [
    CommonModule,
    IonicModule.forRoot(),
      PipesModule
  ],
  exports: [PlayerComponent, PlayerMatchStatsComponent],
  entryComponents: [],
})
export class ComponentsModule { }
