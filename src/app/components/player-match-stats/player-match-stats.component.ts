import {PlayerTierResponse} from 'src/app/pages/tournament-players/player-tier-response';

import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import {PlayerMatchStatsService} from './player-match-stats.service';
import {LoadingController} from '@ionic/angular';

import {PlayerMatchStatsResponse} from './player-match-stats.model';
import {TournamentResponse} from '../../pages/tournaments/tournament.model';
import {TournamentService} from '../../pages/tournaments/tournament.service';

@Component({
    selector: 'app-player-match-stats',
    templateUrl: './player-match-stats.component.html',
    styleUrls: ['./player-match-stats.component.scss']
})
export class PlayerMatchStatsComponent implements OnInit {
    @Input() playerId: number;
    @Input() scheduleNo: number;

    matchStats: PlayerMatchStatsResponse[];
    playerTotals: PlayerTierResponse[];
    events: TournamentResponse[];
    event: TournamentResponse;

    constructor(private playerMatchService: PlayerMatchStatsService,
                private loadingCtrl: LoadingController,
                private service: TournamentService) {
        this.loadTournametsData();
    }
    async loadTournametsData() {
        const loading = await this.loadingCtrl.create();
        await loading.present();
        this.service.getAVPEvents().subscribe(
            res => {
                this.events = res;
                loading.dismiss();
                this.getEvent(this.scheduleNo);
            },
            err => {
                console.log(err);
                loading.dismiss();
            }
        );
    }
    doRefresh(event) {
        this.loadMatchStats();
        this.loadGrandTotalStats();
    }

    refresh(){
        this.loadMatchStats();
        this.loadGrandTotalStats();
    }

    ngOnInit() {
        if(this.scheduleNo){
            this.loadMatchStats();
            this.loadGrandTotalStats();
        }
       else{
           this.scheduleNo=4;
        }
    }

    tournamentChanged(event: any) {

        // Call our service function which returns an Observable
        this.scheduleNo = event.target.value;
        this.loadMatchStats();
        this.loadGrandTotalStats();
        this.getEvent(this.scheduleNo);
    }

    ionViewDidEnter() {
        if(this.scheduleNo){
            this.loadMatchStats();
            this.loadGrandTotalStats();
        }
        else{
            this.scheduleNo=4;
            this.loadMatchStats();
            this.loadGrandTotalStats();
        }
    }

    async loadGrandTotalStats() {
        const loading = await this.loadingCtrl.create({
            message: 'Loading Grand Total Stats...'
        });
        await loading.present();
        console.log(this.scheduleNo);
        this.playerMatchService.getPlayerGrandTotalsByEventNo(this.scheduleNo, this.playerId).subscribe(
            res => {
                console.table('stats', res);
                this.playerTotals = res;
                loading.dismiss();
            },
            err => {
                console.error('load Stats Error', err);
                loading.dismiss();
            }
        );
    }
    getEvent(scheduleNo){

        this.events.forEach((ev) => {

            if (scheduleNo == ev.scheduleNo) {
                this.event = ev;

            }
        });
    }
    async loadMatchStats() {
        const loading = await this.loadingCtrl.create({
            message: 'Loading Fantasy Stats...'
        });
        await loading.present();

        this.playerMatchService.getPlayerMatchStats(this.scheduleNo, this.playerId).subscribe(
            res => {

                this.matchStats = res;
                loading.dismiss();
            },
            err => {
                console.error('load Stats Error', err);
                loading.dismiss();
            }
        );
    }
}
