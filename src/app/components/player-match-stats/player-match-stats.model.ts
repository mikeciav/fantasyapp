export interface PlayerMatchStatsResponse {
    bracket: string;
    matchNo: number;
    matchState: string;
    setsWon: number;
    eventNo: number;
    playerId: number;
    attacks: number;
    kills: number;
    blocks: number;
    digs: number;
    aces: number;
    serviceErrors: number;
    tournamentId: number;
}
