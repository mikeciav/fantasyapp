import { PlayerTierResponse } from 'src/app/pages/tournament-players/player-tier-response';

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PlayerMatchStatsResponse } from './player-match-stats.model';
import {SERVER_URL} from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class PlayerMatchStatsService {
  httpOptions = {
    headers: new HttpHeaders({
      Authorization: '',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };

  constructor(private http: HttpClient) {}


  // DONE
  getPlayerMatchStats(eventNo, playerId): Observable<PlayerMatchStatsResponse[]> {
    return this.http
      .get(`${SERVER_URL}/api/event/${eventNo}/all/matches/player/${playerId}`)
      .pipe(map(results => results as PlayerMatchStatsResponse[]));
  }

  // from the id get the tournaments stats and also the match stats
  getPlayerGrandTotalsByEventNo(eventNo, playerId): Observable<PlayerTierResponse[]> {
    return this.http
      .get(`${SERVER_URL}/api/event/${eventNo}/stats/total/player/${playerId}`)
      .pipe(map(results => results as PlayerTierResponse[]));
  }
}
