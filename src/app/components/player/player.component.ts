import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

import { ModalController } from '@ionic/angular';
import { AthleteProfileCardPage } from 'src/app/pages/athlete-profile-card/athlete-profile-card.page';
import { PlayerTierResponse } from 'src/app/pages/tournament-players/player-tier-response';
import { TournamentResponse } from 'src/app/pages/tournaments/tournament.model';
import { Storage } from '@ionic/storage';
import { componentNeedsResolution } from '@angular/core/src/metadata/resource_loading';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {
  @Input() team: PlayerTierResponse[];
  @Input() player: PlayerTierResponse;
  @Input() tournament: TournamentResponse;
  @Output() selectedPlayer = new EventEmitter<PlayerTierResponse>();
  @Output() removePlayer = new EventEmitter<PlayerTierResponse>();
  isSelected = false;
  dataReturned: any;
  isLoggedIn: boolean;

  constructor(private modalController: ModalController, private storage: Storage) {}

  ngOnInit() {

    this.storage.get('userId').then(val => {
      if (val) {
        this.isLoggedIn = true;
        const index = this.team.findIndex(x => x.playerId === this.player.playerId);
        if (index === -1) {
          this.isSelected = false;
        } else {
          this.isSelected = true;
        }
      } else {
        this.isLoggedIn = false;
      }
    });
    // call the service api to get the fantasy point stats Just the totalfor this player
  }
  selectedPlayerToTeam(selPlayer: PlayerTierResponse) {
    this.selectedPlayer.emit(selPlayer);
    this.isSelected = true;
  }

  removePlayerFromTeam(selPlayer: PlayerTierResponse) {
    this.removePlayer.emit(selPlayer);
    this.isSelected = false;
  }
  async openModal(player: PlayerTierResponse) {
    const modal = await this.modalController.create({
      component: AthleteProfileCardPage,
      componentProps: {
        paramID: player.playerId,
        paramName: player.name,
        paramImg: player.imageUrl,
        paramScheduleNo: this.tournament.scheduleNo,
        paramLocation: this.tournament.location
      }
    });

    modal.onDidDismiss().then(dataReturned => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
      }
    });

    return await modal.present();
  }
}
