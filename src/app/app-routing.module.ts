import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {TutorialGuard} from './guards/tutorial.guard';
import {InternalServerComponent} from './error-pages/internal-server/internal-server.component';
import {NotFoundComponent} from './error-pages/not-found/not-found.component';


const routes: Routes = [
    {path: '', redirectTo: 'app/home', pathMatch: 'full'},
    {
        path: 'login',
        loadChildren: './pages/login/login.module#LoginPageModule'
    },
    {path: 'tutorial', loadChildren: './pages/tutorial/tutorial.module#TutorialPageModule'},
    {path: 'app', loadChildren: './pages/tabs/tabs.module#TabsPageModule'},
    {path: 'signup', loadChildren: './pages/signup/signup.module#SignupPageModule'},
    {
        path: 'athlete-profile-card',
        loadChildren: './pages/athlete-profile-card/athlete-profile-card.module#AthleteProfileCardPageModule'
    },
    {path: '500', component: InternalServerComponent},
    { path: '404', component : NotFoundComponent},
    { path: '**', redirectTo: '/404', pathMatch: 'full'}
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule],
    declarations: [InternalServerComponent,NotFoundComponent]
})
export class AppRoutingModule {
}
