import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {SortPipe} from './sort.pipe';
import {SearchPipe} from './search.pipe';
import {CustomFormatterPipe} from './custom-formatter.pipe';


@NgModule({
    declarations: [SortPipe, SearchPipe, CustomFormatterPipe],
    imports: [CommonModule,
        IonicModule.forRoot()],
    exports: [SortPipe, SearchPipe, CustomFormatterPipe]
})
export class PipesModule {
}