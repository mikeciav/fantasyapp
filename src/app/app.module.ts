import { AthleteProfileCardPageModule } from './pages/athlete-profile-card/athlete-profile-card.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { ValidatorsModule } from './validators/validators.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import {PipesModule} from './pipes/pipes.module';
import {EnvironmentUrlService} from './shared/services/environment-url.service';
import { ErrorHandlerService } from './shared/services/error-handler.service';
import {SharedModule} from './shared/shared.module';
import {DecimalPipe} from '@angular/common';


library.add(fab);

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    FontAwesomeModule,
    ValidatorsModule,
    AthleteProfileCardPageModule,
      PipesModule,
      SharedModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })],

  providers: [
    StatusBar,
    SplashScreen,
      EnvironmentUrlService,
      ErrorHandlerService,
      DecimalPipe,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }

  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
