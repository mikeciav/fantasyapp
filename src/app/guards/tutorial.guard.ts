import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class TutorialGuard implements CanActivate  {
  constructor(private storage: Storage, private router: Router) { }

  // must add async with await
  async canActivate(): Promise<boolean> {
    // by adding await it waits until storage is returned
    const hasSeen = await this.storage.get('tutorialSeen');

    if (!hasSeen) {
      this.router.navigateByUrl('/login');
    }

    return hasSeen;
  }
}
