import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AthleteProfileCardPage } from './athlete-profile-card.page';
import { ComponentsModule } from 'src/app/components/components.module';
import {PipesModule} from '../../pipes/pipes.module';

const routes: Routes = [
  {
    path: '',
    component: AthleteProfileCardPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes),
      PipesModule
  ],
  declarations: [AthleteProfileCardPage]
})
export class AthleteProfileCardPageModule {}
