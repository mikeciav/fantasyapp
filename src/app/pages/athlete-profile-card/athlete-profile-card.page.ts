import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-athlete-profile-card',
  templateUrl: './athlete-profile-card.page.html',
  styleUrls: ['./athlete-profile-card.page.scss']
})
export class AthleteProfileCardPage implements OnInit {
  constructor(private modalController: ModalController, private navParams: NavParams, private loadingCtrl: LoadingController) {
      console.log( 'con parmid', this.navParams.get('paramID'));
      console.log('con parmaname', this.navParams.get('paramName'));
      this.playerId = this.navParams.get('paramID');
      this.playerName = this.navParams.get('paramName');
      this.playerImage = this.navParams.get('paramImg');
      this.location = this.navParams.get('paramLocation');
      this.scheduleNo = this.navParams.get('paramScheduleNo');

  }
  playerName = '';
  playerId = 0;
  playerImage = 'NA';
  location = '';
  scheduleNo = '';

  async ngOnInit() {
    let loading;
    loading = await this.loadingCtrl.create({
      message: 'Loading Athlete....',
      spinner: 'circles',
      showBackdrop: true,
      duration: 60000
    });
    await loading.present();

    this.playerId = this.navParams.data.paramID;
    this.playerName = this.navParams.data.paramName;
    this.playerImage = this.navParams.data.paramImg;
    this.location = this.navParams.data.paramLocation;
    this.scheduleNo = this.navParams.data.paramScheduleNo;

    loading.dismiss();
  }

    ionViewWillEnter() {
        this.playerId = this.navParams.get('paramID');
        this.playerName = this.navParams.get('paramName');
        this.playerImage = this.navParams.get('paramImg');
        this.location = this.navParams.get('paramLocation');
        this.scheduleNo = this.navParams.get('paramScheduleNo');
    }

  async closeModal() {
    const onClosedData: string = 'Wrapped Up!';
    await this.modalController.dismiss(onClosedData);
  }
}
