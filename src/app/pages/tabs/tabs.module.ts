import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '',
    component: TabsPage,
    children: [
      { path: 'home', loadChildren: '../home/home.module#HomePageModule' },      
      { path: 'tournaments', loadChildren: '../tournaments/tournaments.module#TournamentsPageModule' },
      { path: 'team', loadChildren: '../fantasy-team/fantasy-team.module#FantasyTeamPageModule' },
      { path: 'athlete-tiers', loadChildren: '../athlete-tiers/athlete-tiers.module#AthleteTiersPageModule' },
      { path: 'menu', loadChildren: '../menu/menu.module#MenuPageModule' },
      { path: 'leaderboard', loadChildren: '../leader-board/leader-board.module#LeaderBoardPageModule'},
      { path: 'profile', loadChildren: '../profile/profile.module#ProfilePageModule' },
      { path: 'tournament-players', loadChildren: '../tournament-players/tournament-players.module#TournamentPlayersPageModule' },
      { path: 'research-players', loadChildren: '../research-players/research-players.module#ResearchPlayersPageModule' },
  
    ]
  }
];

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, RouterModule.forChild(routes)],
  declarations: [TabsPage]
})
export class TabsPageModule {}
