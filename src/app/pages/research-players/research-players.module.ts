import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ResearchPlayersPage } from './research-players.page';
import { ComponentsModule } from 'src/app/components/components.module';
import {PipesModule} from 'src/app/pipes/pipes.module';


const routes: Routes = [
  {
    path: '',
    component: ResearchPlayersPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule,
      PipesModule
  ],
  declarations: [ResearchPlayersPage]
})
export class ResearchPlayersPageModule {}
