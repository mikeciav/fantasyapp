import {Component, OnInit} from '@angular/core';
import {Storage} from '@ionic/storage';
import {TournamentPlayerService} from '../tournament-players/tournament-players.service';
import {Router, ActivatedRoute} from '@angular/router';
import {LoadingController, AlertController, ModalController} from '@ionic/angular';
import {TournamentResponse} from '../tournaments/tournament.model';
import {PlayerTierResponse} from '../tournament-players/player-tier-response';
import {AthleteProfileCardPage} from '../athlete-profile-card/athlete-profile-card.page';
import {TournamentService} from '../tournaments/tournament.service';

@Component({
    selector: 'app-research-players',
    templateUrl: './research-players.page.html',
    styleUrls: ['./research-players.page.scss']
})
export class ResearchPlayersPage implements OnInit {
    event: TournamentResponse;
    events: TournamentResponse[];
    athleteTierList: PlayerTierResponse[];
    scheduleNo: number = 0;
    declareAthletes: PlayerTierResponse[];
    descending = false;
    order: number;
    column = 'seed';
    selectedScheduleName;
    loaded=false;
    constructor(
        private tournamentPlayerService: TournamentPlayerService,
        private route: ActivatedRoute,
        private router: Router,
        private loadingCtrl: LoadingController,
        private storage: Storage,
        private alertCtrl: AlertController,
        private modalController: ModalController,
        private service: TournamentService
    ) {
        this.route.queryParams.subscribe(params => {

            this.event = new TournamentResponse();
            if (this.router.getCurrentNavigation().extras.state) {

                this.event = this.router.getCurrentNavigation().extras.state.tournament;

                this.scheduleNo = this.event.scheduleNo;
                this.loadAthletes(this.scheduleNo);
                console.log('constructor', this.event);
                this.selectedScheduleName = this.event.eventName;
            }
            else {
                console.log('in constructor');
                    this.event.eventName = 'All Event Stats';
                    this.event.scheduleNo = 0;
                    this.event.location = '';
                    this.event.dateRange = '';
                    this.event.status = 0;
                    this.selectedScheduleName = this.event.eventName;
                this.loadAthletes(this.event.scheduleNo );
            }
        });

        this.loadTournametsData();
    }

    ascDesc(){
        this.descending = !this.descending;
        this.order = this.descending ? 1 : -1;


    }
    sort(sortField:any){
        this.column =  sortField.target.value;
        this.descending = false;
        this.order = this.descending ? 1 : -1;


    }

    doRefresh(event) {
        this.loadAthletes(this.scheduleNo);
        console.log(this.scheduleNo);
        setTimeout(() => {
            console.log('Async operation has ended');
            event.target.complete();
        }, 2000);
    }

    async loadTournametsData() {
        this.service.getAVPEvents().subscribe(
            res => {
                console.log('events', res);
                this.events = res;

            },
            err => {
                console.log(err);

            }
        );
    }

    ngOnInit() {
        this.loadTournametsData();

    }

    loadDeclareAthletes(eventNo) {
        try {
            this.tournamentPlayerService.getResearchTournamentPlayersLiveStats(eventNo).subscribe(
                res => {
                    this.declareAthletes = res;

                },
                err => {
                    console.log(err);

                }
            );
        } catch (err) {

            console.log(err);
        }
    }

    ionViewDidLoad() {
        if (this.event) {
            this.scheduleNo = this.event.scheduleNo;
            this.selectedScheduleName = this.event.eventName;
            this.loadAthletes(this.event.scheduleNo);
        } else {
            this.scheduleNo = 1;
            this.loadAthletes(1);
        }
    }

    filterAthletes(parms: any): void {


        const val: string = parms;

        this.athleteTierList = this.declareAthletes.filter((item) => {
            if(item.partnerName){
                return item.name.toLocaleLowerCase().indexOf(val.toLocaleLowerCase()) > -1 ||
                    item.partnerName.toLocaleLowerCase().indexOf(val.toLocaleLowerCase()) > -1;
            }else {
                return item.name.toLocaleLowerCase().indexOf(val.toLocaleLowerCase()) > -1;
            }


        });
       /* if (this.scheduleNo) {
            const val: string = parms;
            this.tournamentPlayerService.getResearchTournamentPlayersLiveStats(this.scheduleNo).subscribe(
                res => {

                    this.declareAthletes = res;


                    this.athleteTierList = this.declareAthletes.filter((item) => {
                        return item.name.toLocaleLowerCase().indexOf(val.toLocaleLowerCase()) > -1 ||
                            item.partnerName.toLocaleLowerCase().indexOf(val.toLocaleLowerCase()) > -1;
                    });
                },
                err => {
                    console.log(err);

                }
            );

        }*/
    }

    tournamentChanged(selEvent: any) {
        this.event = new TournamentResponse();
        // Call our service function which returns an Observable
        this.scheduleNo = Number(selEvent.detail.value);
        if (this.events) {

            if (this.scheduleNo <= 0) {
                this.event.eventName = 'All Event Stats';
                this.event.scheduleNo = 0;
                this.event.location = '';
                this.event.dateRange = '';
                this.event.status = 0;
            }
            else {
                this.getEvent(this.scheduleNo);
            }

            this.selectedScheduleName = this.event.eventName;
        }
        console.log('tchanged', this.event);
        this.loadAthletes(this.scheduleNo);

    }

    getEvent(scheduleNo) {

        this.events.forEach((ev) => {

            if (scheduleNo == ev.scheduleNo) {
                this.event = ev;

            }
        });
    }

    async openModal(player: PlayerTierResponse) {

        const modal = await this.modalController.create({
            component: AthleteProfileCardPage,
            componentProps: {
                paramID: player.playerId,
                paramName: player.name,
                paramImg: player.imageUrl,
                paramScheduleNo: this.scheduleNo,
                paramLocation: "location"
            }
        });

        modal.onDidDismiss().then(dataReturned => {
            if (dataReturned !== null) {
                //this.dataReturned = dataReturned.data;
            }
        });

        return await modal.present();
    }

    async loadAthletes(t) {
        let loading;
        loading = await this.loadingCtrl.create({
            message: 'Loading Athletes....',
            spinner: 'circles',
            showBackdrop: true,
            duration: 60000
        });
        await loading.present();
        try {
            this.tournamentPlayerService.getResearchTournamentPlayersLiveStats(t).subscribe(
                res => {
                    this.athleteTierList = res.sort((a, b) => (a.grandPointsTotal > b.grandPointsTotal ? -1 : 1));
                    this.column = 'grandPointsTotal';
                    this.loaded = true;
                    this.declareAthletes = this.athleteTierList;
                    loading.dismiss();
                },
                err => {
                    console.log(err);
                    loading.dismiss();
                }
            );
        } catch (err) {
            loading.dismiss();
            this.router.navigate(['app/tournaments']);
        }
    }

    logOut() {
        this.storage.clear().then(v => {
            this.router.navigate(['login']);
        });
    }
}
