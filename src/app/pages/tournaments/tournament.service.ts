import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment, SERVER_URL} from '../../../environments/environment';

import {TournamentModel, TournamentResponse} from './tournament.model';

@Injectable({
    providedIn: 'root'
})
export class TournamentService {
    // Resolve HTTP using the constructor
    constructor(private http: HttpClient) {
    }
    /**
     * Get data from the Tournaments API
     * map the result to return only the results that we need
     * @returns Observable with the search results
     * ${this.url}/${eventNo}/all/players/
     */
    getAVPEvents(): Observable<TournamentResponse[]> {
        return this.http.get(`${SERVER_URL}/api/event/all`).pipe(map(results => results as TournamentResponse[]));
    }
}
