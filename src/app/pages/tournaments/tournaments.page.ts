import {Component, OnInit} from '@angular/core';

import {LoadingController} from '@ionic/angular';
import {Router, NavigationExtras} from '@angular/router';
import {TournamentResponse} from './tournament.model';
import {TournamentService} from './tournament.service';
import {Observable} from 'rxjs';
import {Storage} from '@ionic/storage';
import {RepositoryService} from '../../shared/services/repository.service';
import {ErrorHandlerService} from '../../shared/services/error-handler.service';

@Component({
    selector: 'app-tournaments',
    templateUrl: './tournaments.page.html',
    styleUrls: ['./tournaments.page.scss']
})
export class TournamentsPage implements OnInit {
    events: TournamentResponse[];
    errorMessage: string;

    constructor(
        private service: TournamentService,
        private router: Router,
        private loadingCtrl: LoadingController,
        private storage: Storage,
        private repository: RepositoryService,
        private errorHandler: ErrorHandlerService
    ) {
    }

    ngOnInit() {
        this.loadData();
    }

    ionViewDidEnter() {
        this.loadData();
    }

    // https://www.djamware.com/post/5b94bb1d80aca74669894415/ionic-4-angular-6-tutorial-call-multiple-services-at-once
    async loadData() {
        const loading = await this.loadingCtrl.create();
        await loading.present();

        let apiAddress: string = 'api/event/all';
        this.repository.getData(apiAddress)
            .subscribe(res => {
                    this.events = res as TournamentResponse[];
                    loading.dismiss();
                },
                (error) => {
                    this.errorHandler.handleError(error);
                    this.errorMessage = this.errorHandler.errorMessage;
                    console.error('ERROR FROM TOURNAMENT', this.errorMessage);
                    loading.dismiss();

                });


        /*this.service.getAVPEvents().subscribe(
          res => {
            console.log('events', res);
            this.events = res;
            loading.dismiss();
          },
          err => {
            console.log(err);
            loading.dismiss();
          }
        );*/
    }

    logOut() {
        this.storage.clear().then(v => {
            this.router.navigate(['login']);
        });
    }

    open(n: TournamentResponse) {
        const navigationExtras: NavigationExtras = {
            state: {
                tournament: n
            }
        };

        if (n.status === 2) {
            this.router.navigate(['app/tournament-players'], navigationExtras);
        } else {
            this.router.navigate(['app/leaderboard'], navigationExtras);
        }
    }
}
