import { AthleteViewModel } from './athleteTierModel';

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserFantasyTeamRequest } from '../tournament-players/player-tier-response';

@Injectable({
  providedIn: 'root'
})
export class AthleteTierService {
  httpOptions = {
    headers: new HttpHeaders({
      Authorization: '',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };
  // private instance variable to hold base url
  private url = 'https://fantasyapi.herokuapp.com/api/tournament/athletes/';
  //private url = 'http://localhost:5000/api/tournament/athletes/'

  private apiUrl = 'https://fantasyapi.herokuapp.com/api/fantasyTeam';
  constructor(private http: HttpClient) {}

  /// ser/{userId}/scheduleNo/{scheduleNo}
  getUsersFantasyTeam(scheduleNo, userId): Observable<AthleteViewModel[]> {
    return this.http.get(`${this.apiUrl}/user/${userId}/scheduleNo/${scheduleNo}`).pipe(map(results => results as AthleteViewModel[]));
  }

  getAthletesByScheduleNo(scheduleNo): Observable<AthleteViewModel[]> {
    // `${this.url}/top-headlines?category=${cat}&country=us&apiKey=${this.apiKey}`

    return this.http.get(`${this.url}${scheduleNo}`).pipe(map(results => results as AthleteViewModel[]));
  }

  //  /delete/u/{userId/p/{playerId}/s/{scheduleNo}/t/{tier}
  removeAthleteFromTeam(data: UserFantasyTeamRequest) {
    console.log('Remove Athlete From Fantasy Team:', data);
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      data
    };

    return new Promise((resolve, reject) => {
      this.http.delete(`${this.apiUrl}/delete/u/${data.userId}/p/${data.playerId}/s/${data.scheduleNo}/t/${data.tier}`, options).subscribe(
        s => {
          resolve(s);
        },
        err => {
          reject(err);
        }
      );
    }).catch(this.handleError);
  }

  addAthleteToTeam(data: UserFantasyTeamRequest) {
    console.log('Add Athlete to Fantasy Team:', data);

    return new Promise((resolve, reject) => {
      this.http
        .post(`${this.apiUrl}/add`, JSON.stringify(data), {
          headers: new HttpHeaders({
            Authorization: '',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    }).catch(this.handleError);
  }
  private handleError(error: any): Promise<any> {
    console.log(error);
    return Promise.reject(error.message || error);
  }

  //
}
