export interface AthleteViewModel {
  id: number;
  seed: number;
  fullName: string;
  tournamentId: number;
  tier: number;
  scheduleNo: number;
  avp_player_id: number;
  image_url: string;
  partnerName: string;
  finalResult: number;
  seedDiff: number;
  fantasyResultAwardBonus: number;
  seedDiffAwardBonus: number;
  statsFantasyPoints: number;
  grandTotal: number;
  playerId: number;
  gender: string;
  fantasy_attacks: number;
  fantasy_kills: number;
  fantasy_blocks: number;
  fantasy_digs: number;
  fantasy_aces: number;
  fantasy_service_errors: number;
}


/*    */

/* <ion-select  (ionChange)="tournamentChanged($event)">
<ion-select-option value="">Tournament</ion-select-option>
<ion-select-option value="2">2019 Austin Open</ion-select-option>
<ion-select-option value="1">2019 Huntington Beach Open</ion-select-option>    
</ion-select>   */