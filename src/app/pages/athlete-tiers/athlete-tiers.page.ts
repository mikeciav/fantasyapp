import { AthleteViewModel } from './athleteTierModel';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { TournamentModel } from '../tournaments/tournament.model';
import { Storage } from '@ionic/storage';
import { AthleteTierService } from './athlete-tiers.service';
import { LoadingOptions } from '@ionic/core';
import { UserFantasyTeamRequest } from '../tournament-players/player-tier-response';

@Component({
  selector: 'app-athlete-tiers',
  templateUrl: './athlete-tiers.page.html',
  styleUrls: ['./athlete-tiers.page.scss']
})
export class AthleteTiersPage implements OnInit {
  tournament: TournamentModel;
  selected = '1';
  athleteTier: AthleteViewModel[];
  athleteTierList: AthleteViewModel[];
  userTeam: AthleteViewModel[];
  athleteFantasy: UserFantasyTeamRequest = new UserFantasyTeamRequest();
  userId: any = 0;
  tier1Count: any = 0;
  tier2Count: any = 0;
  tier3Count: any = 0;
  tier4Count: any = 0;

  tierSelected: any = 1;

  slidesOpts = {
    slidesPerView: 6,
    freeMode: true,
    centeredSlides: false
  };

  constructor(
    private data: AthleteTierService,
    private route: ActivatedRoute,
    private router: Router,
    private loadingCtrl: LoadingController,
    private storage: Storage
  ) {
    // has to be in contructor
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.tournament = this.router.getCurrentNavigation().extras.state.tournament;
      } else {
        this.router.navigate(['app/tournaments']);
      }
    });
  }

  onSelected(player: AthleteViewModel){
    console.log('playerfromcomponent',player);
   // this.addToTeam(player);
  }
  onRemoved(player: AthleteViewModel) {
    console.log('player Removed From Team', player);
    //this.removeFromTeam(player);
  }

  ngOnInit() {}

  setTeamTierCount() {
    if (this.userTeam) {
      this.tier1Count = this.userTeam.filter((value: AthleteViewModel) => value.tier === 1).length;
      this.tier2Count = this.userTeam.filter((value: AthleteViewModel) => value.tier === 2).length;
      this.tier3Count = this.userTeam.filter((value: AthleteViewModel) => value.tier === 3).length;
      this.tier4Count = this.userTeam.filter((value: AthleteViewModel) => value.tier === 4).length;
    }
  }

  async loadFantasyTeam() {
    const loading = await this.loadingCtrl.create({
      message: 'Getting Your Fantasy Team...'
    });
    await loading.present();

    this.userId = await this.storage.get('userId');
    this.data.getUsersFantasyTeam(2, this.userId).subscribe(
      res => {
        console.log('user team', res);
        this.userTeam = res;
        this.setTeamTierCount();
        loading.dismiss();
      },
      err => {
        console.log('getuserfantasyerror', err);
        loading.dismiss();
      }
    );
  }

  async loadAthletes(t) {
    let loading;
    loading = await this.loadingCtrl.create({
      message: 'Loading Athletes....',
      spinner: 'circles',
      showBackdrop: true,
      duration: 60000
    });
    try {
      if (this.tournament.status === 2 || this.tournament.status === 3) {
        loading = await this.loadingCtrl.create({
          message: this.tournament.location + ' Is Live Gathering Realtime Stats. Please Wait.',
          spinner: 'circles',
          showBackdrop: true,
          duration: 90000
        });
      }
    } catch (err) {}

    await loading.present();
    try {
      this.data.getAthletesByScheduleNo(this.tournament.scheduleNo).subscribe(
        res => {
          this.athleteTierList = res;
          this.athleteTier = this.athleteTierList.filter((value: AthleteViewModel) => value.tier === t);
          loading.dismiss();
        },
        err => {
          console.log(err);
          loading.dismiss();
        }
      );
    } catch (err) {
      loading.dismiss();
      this.router.navigate(['app/tournaments']);

    }
  }

  ionViewDidEnter() {
    this.loadAthletes(1);
   // this.loadFantasyTeam();
  }

  doRefresh(event) {
    this.loadAthletes(this.tierSelected);
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }
  async addToTeam(player: AthleteViewModel) {
    if (this.tournament.status !== 2) {
      return;
    }
    const msg = 'Adding' + player.fullName + ' to your team! :)';
    const loading = await this.loadingCtrl.create({
      message: msg,
      spinner: 'circles',
      showBackdrop: true,
      duration: 60000
    });
    await loading.present();

    // only add 4 players per tier.
    if (this.userTeam.filter((value: AthleteViewModel) => value.tier === player.tier).length === 4) {
      alert('Only 4 allowed');
      loading.dismiss();
    } else {
      const index = this.userTeam.findIndex(x => x.playerId === player.playerId);
      // here you can check specific property for an object whether it exist in your array or not
      if (index === -1) {
        // add player to database
        try {
          this.athleteFantasy = new UserFantasyTeamRequest();

          this.athleteFantasy.playerId = player.playerId;
          this.athleteFantasy.scheduleNo = player.scheduleNo;
          this.athleteFantasy.tier = player.tier;
          this.athleteFantasy.userId = this.userId;

          this.data
            .addAthleteToTeam(this.athleteFantasy)
            .then(res => {
              console.log('Add Team Success', res);
              this.userTeam.push(player);
              this.setTeamTierCount();
              loading.dismiss();
            })
            .catch(err => {
              console.log('Error in Add Team', err);
              loading.dismiss();
              this.router.navigate(['app/tournaments']);
            });
        } catch (ex) {
          console.log('ERROR adding player', ex);
          loading.dismiss();
          this.router.navigate(['app/tournaments']);
        }
      } else {
        alert('Player Already Exists On Your Team');
        loading.dismiss();
      }
    }
  }
  async removeFromTeam(player: AthleteViewModel) {
    if (this.tournament.status !== 2) {
      return;
    }
    const msg = 'Removing ' + player.fullName + ' from your team. :( boo';
    const loading = await this.loadingCtrl.create({
      message: msg,
      spinner: 'circles',
      showBackdrop: true,
      duration: 60000
    });
    await loading.present();
    try {
      this.athleteFantasy = new UserFantasyTeamRequest();
      this.athleteFantasy.playerId = player.playerId;
      this.athleteFantasy.scheduleNo = player.scheduleNo;
      this.athleteFantasy.tier = player.tier;
      this.athleteFantasy.userId = this.userId;

      this.data
        .removeAthleteFromTeam(this.athleteFantasy)
        .then(res => {
          console.log('Remove Team Success', res);
          const index = this.userTeam.indexOf(player);
          this.userTeam.splice(index, 1);
          this.setTeamTierCount();
          loading.dismiss();
        })
        .catch(err => {
          console.log('Error in Add Team', err);
          loading.dismiss();
          this.router.navigate(['app/tournaments']);
        });
    } catch (ex) {
      console.log('TRY ERROR IN REMOVE', ex);
      loading.dismiss();
      this.router.navigate(['app/tournaments']);
    }
  }

  segmentChanged(event) {
    this.tierSelected = Number(event);
    if (this.tournament.status === 3) {
      this.loadAthletes(event);
    } else {
      this.athleteTier = this.athleteTierList.filter((value: AthleteViewModel) => value.tier === Number(event));
    }
  }

  open() {
    const navigationExtras: NavigationExtras = {
      state: {
        tournament: this.tournament
      }
    };
    this.router.navigate(['app/team'], navigationExtras);
  }
}
