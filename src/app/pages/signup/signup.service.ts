import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SignUpRequest } from './signup.model';
import {SERVER_URL} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  constructor(private http: HttpClient) {}

  signupUser(data: SignUpRequest) {
    console.log('signupUser:', data);

    return new Promise((resolve, reject) => {
      this.http
        .post(`${SERVER_URL}/api/auth/signup`, JSON.stringify(data), {
          headers: new HttpHeaders({
            Authorization: '',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
          })
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    }).catch(this.handleError);
  }
  private handleError(error: any): Promise<any> {
    console.log(error);
    return Promise.reject(error.message || error);
  }
}
