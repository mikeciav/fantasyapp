import { SignUpRequest } from './signup.model';
import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { PasswordValidator } from 'src/app/validators/password.validator';
import { UsernameValidator } from 'src/app/validators/username.validator';
import { SignupService } from './signup.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss']
})
export class SignupPage implements OnInit {
  constructor(
    public signupService: SignupService,
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    private router: Router,
    private loadingCtrl: LoadingController
  ) {
    this.matching_passwords_group = new FormGroup(
      {
        password: new FormControl(
          '',
          Validators.compose([
            Validators.minLength(4),
            Validators.required,
            Validators.pattern('^[_A-z0-9]*((-|s)*[_A-z0-9])*$')
            //
          ])
        ),
        confirm_password: new FormControl('', Validators.required)
      },
      (formGroup: FormGroup) => {
        return PasswordValidator.areEqual(formGroup);
      }
    );
    this.validations_form = this.formBuilder.group({
      username: new FormControl(
        '',
        Validators.compose([
          UsernameValidator.validUsername,
          Validators.maxLength(25),
          Validators.minLength(5),
          Validators.pattern('^(?=.*[a-zA-Z])[a-zA-Z]+$'),
          Validators.required
        ])
      ),
      name: new FormControl('', Validators.required),
      email: new FormControl(
        '',
        Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])
      ),
      matching_passwords: this.matching_passwords_group,
      terms: new FormControl(true, Validators.pattern('true'))
    });
  }
  // tslint:disable-next-line:variable-name
  validations_form: FormGroup;
  // tslint:disable-next-line:variable-name
  matching_passwords_group: FormGroup;

  // tslint:disable-next-line:variable-name
  validation_messages = {
    username: [
      { type: 'required', message: 'Username is required.' },
      { type: 'minlength', message: 'Username must be at least 5 characters long.' },
      { type: 'maxlength', message: 'Username cannot be more than 25 characters long.' },
      { type: 'pattern', message: 'Your username must contain only numbers and letters.' },
      { type: 'validUsername', message: 'Your username has already been taken.' }
    ],
    name: [{ type: 'required', message: 'Name is required.' }],

    email: [{ type: 'required', message: 'Email is required.' }, { type: 'pattern', message: 'Enter a valid email.' }],
    password: [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 4 characters long.' },
      { type: 'pattern', message: 'Your password must not have any special chars.' }
    ],
    confirm_password: [{ type: 'required', message: 'Confirm password is required' }],
    matching_passwords: [{ type: 'areEqual', message: 'Password mismatch' }],
    terms: [{ type: 'pattern', message: 'You must accept terms and conditions.' }]
  };

  ngOnInit() {}
  ionViewWillLoad() {}
  async onSubmit(values) {
    const loading = await this.loadingCtrl.create({
      message: 'Registering Please Wait. Once complete Please Login.'
    
    });
    await loading.present();
    const signupRequest: SignUpRequest = new SignUpRequest();

    signupRequest.username = values.username;
    signupRequest.email = values.email;
    signupRequest.name = values.name;
    signupRequest.password = values.matching_passwords.password;

    this.signupService
      .signupUser(signupRequest)
      .then(res => {
        loading.dismiss();
        this.router.navigate(['login']);
      })
      .catch(error => {
        loading.dismiss();
        alert('Please Try Again A Error Has Occured! ');

        console.log(error);
      });
  }
}
