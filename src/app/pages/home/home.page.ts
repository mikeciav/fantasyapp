import {Component, OnInit} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {Storage} from '@ionic/storage';
import {Router} from '@angular/router';


@Component({
    selector: 'app-home',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {


    constructor(private sanitizer: DomSanitizer, private storage: Storage, private router: Router) {
    }

    ngOnInit() {

    }



    logOut() {



        this.storage.clear().then(v => {
            this.router.navigate(['login']);
        });
    }
}
