import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { Storage } from '@ionic/storage';
import { LoginRequest } from './login.model';
import { Router } from '@angular/router';
import { Platform, AlertController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  username: string;
  password: string;

  constructor(
    private loginService: LoginService,
    private storage: Storage,
    private router: Router,
    private platform: Platform,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController
  ) {}

  ngOnInit() {}
  ionViewDidEnter() {
    this.storage.get('userId').then(res => {
      if (res) {
        this.router.navigate(['app/tournaments']);
      }
    });
  }
  async showPlatform() {
    const text = 'I run on: ' + this.platform.platforms();
    let alert = await this.alertCtrl.create({
      header: 'Welcome To BVFL!',
      message: text,
      buttons: ['Ok']
    });
    return await alert.present();
  }
  login() {
    if (this.username && this.password) {
      const loginRequest: LoginRequest = new LoginRequest();
      loginRequest.password = this.password;
      loginRequest.usernameOrEmail = this.username;
      console.log(loginRequest);

      this.loginService
        .signIn(loginRequest)
        .then(res => {
          if (res.userId) {
            this.finish(res);
          }
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      alert('nope');
    }
  }
  async finish(result) {
    const loading = await this.loadingCtrl.create({
      message: 'Welcome TO BVFL :) ...'
    });
    await loading.present();
    await this.storage.set('userId', result.userId);
    loading.dismiss();
    this.router.navigate(['app/tournaments']);
  }
}
