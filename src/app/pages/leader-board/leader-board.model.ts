import { AthleteViewModel } from '../athlete-tiers/athleteTierModel';

export class LeaderBoardResponse {
  userName: string;
  userId: number;
  grandTotalPoints: number;
  noPlayersEliminated: number;
    avgMultiplier:number;
}
