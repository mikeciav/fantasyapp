import { LeaderBoardResponse } from './leader-board.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PlayerTierResponse } from '../tournament-players/player-tier-response';
import {SERVER_URL} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LeaderBoardService {
  httpOptions = {
    headers: new HttpHeaders({
      Authorization: '',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };

  constructor(private http: HttpClient) {}

  getLeaderBoardByScheduleNo(scheduleNo): Observable<LeaderBoardResponse[]> {
    return this.http.get(`${SERVER_URL}/api/event/${scheduleNo}/leaderBoard/`).pipe(map(results => results as LeaderBoardResponse[]));
  }

  // Get Number of Players Left in tournament by userid
  
}
