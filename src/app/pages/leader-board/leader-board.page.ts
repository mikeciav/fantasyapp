import {PlayerTierResponse} from './../tournament-players/player-tier-response';
import {AlertController, LoadingController} from '@ionic/angular';
import {LeaderBoardService} from './leader-board.service';
import {Component, OnInit} from '@angular/core';
import {LeaderBoardResponse} from './leader-board.model';
import {NavigationExtras, Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {TournamentResponse} from '../tournaments/tournament.model';
import {TournamentService} from '../tournaments/tournament.service';

@Component({
    selector: 'app-leader-board',
    templateUrl: './leader-board.page.html',
    styleUrls: ['./leader-board.page.scss']
})
export class LeaderBoardPage implements OnInit {
    leaderBoardResponse: LeaderBoardResponse[];
    scheduleNo = 4;
    userId = 0;
    events: TournamentResponse[];
    event: TournamentResponse;
    loaded = false;
    loggedInUsersTeam: LeaderBoardResponse;
    place =0;
    constructor(
        private leaderBoardService: LeaderBoardService,
        private loadingCtrl: LoadingController,
        private router: Router,
        private storage: Storage,
        private service: TournamentService,
        private alertCtrl: AlertController
    ) {

    }

    tournamentChanged(event: any) {
        console.log(event.target.value);
        // Call our service function which returns an Observable
        this.scheduleNo = event.target.value;
        this.getLeaderBoard(this.scheduleNo);

    }

    ngOnInit() {
        this.getLeaderBoard(this.scheduleNo);
        this.loadTournametsData();
    }

    async doRefresh(event) {
        this.getLeaderBoard(this.scheduleNo);
        setTimeout(() => {
            console.log('Refreshing Leader board');
            event.target.complete();
        }, 1000);
    }

    async getLeaderBoard(scheduleNo) {
        this.loadTournametsData();
        const msg = 'Calculating Leader Board. Please Wait :)';
        const loading = await this.loadingCtrl.create({
            message: msg,
            spinner: 'circles',
            showBackdrop: true,
            duration: 60000
        });
        await loading.present();
        this.leaderBoardService.getLeaderBoardByScheduleNo(scheduleNo).subscribe(
            res => {

                this.leaderBoardResponse = res;
                this.leaderBoardResponse.sort((a, b) => (a.grandTotalPoints > b.grandTotalPoints ? -1 : 1));

                //get users team
                try {
                    this.loggedInUsersTeam = this.leaderBoardResponse.filter((val) => {
                        return (val.userId === this.userId);
                    })[0];


                    if (this.loggedInUsersTeam) {
                        this.place =  this.leaderBoardResponse.findIndex((i) => {
                            return (i.userId === this.userId);
                        });

                        this.loaded = true;
                    }else{
                        this.loaded=false;
                    }
                }
                catch (e) {
                    console.log(e);
                    this.loaded=false;
                }
                this.getEvent(scheduleNo).then((val) => {
                    this.scheduleNo = scheduleNo;
                });

                loading.dismiss();
            },
            err => {
                console.log(err);
                loading.dismiss();
            }
        );
    }

    goToUserFantasyTeam(eventNumber, userId, leaderBoardResponse) {


        const navigationExtras: NavigationExtras = {
            state: {
                eventNo: eventNumber,
                user: userId,
                leaderResponse: leaderBoardResponse,
                tournament: this.event
            }
        };
        this.getEvent(eventNumber).then((val) => {
            if (this.event.status !== 2) {
                this.router.navigate(['app/team'], navigationExtras);
            }
            else if (this.event.status === 2 && this.userId === userId) {
                this.router.navigate(['app/team'], navigationExtras);
            } else {
                this.eventNotOpenError();
            }
        })
        ;


    }

    async getEvent(scheduleNo) {

        this.events.forEach((ev) => {

            if (scheduleNo == ev.scheduleNo) {
                this.event = ev;

            }
        });
        return await this.event;
    }

    async eventNotOpenError() {

        const alert = await
            this.alertCtrl.create({
                message: 'Tournament Must Start Before Viewing Other Users Team.',
                buttons: ['Ok']
            });
        alert.present();
    }

    // function for dynamic sorting

    ionViewWillEnter() {
        this.storage.get('userId').then(val => {
            if (val) {
                this.userId = val;
            }
        });
    }

    async loadTournametsData() {

        this.service.getAVPEvents().subscribe(
            res => {

                this.events = res;

            },
            err => {
                console.log(err);

            }
        );
    }

    logOut() {
        this.storage.clear().then(v => {
            this.router.navigate(['login']);
        });
    }
}
