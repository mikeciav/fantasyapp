import {PlayerTierResponse, UserFantasyTeamResponse, UserFantasyTeamRequest} from './player-tier-response';
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment, SERVER_URL} from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class TournamentPlayerService {
    httpOptions = {
        headers: new HttpHeaders({
            Authorization: '',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        })
    };

    constructor(private http: HttpClient) {

    }

    getResearchTournamentPlayersLiveStats(eventNo: number): Observable<PlayerTierResponse[]> {
        let url = `${SERVER_URL}/api/event/${eventNo}/players/live/stats`;
        if (eventNo === 0) {
            url = `${SERVER_URL}/api/event/player/stats/all`;
        }

        return this.http.get(url).pipe(map(results => results as PlayerTierResponse[]));

    }

    getTournamentPlayersByEvent(eventNo: number): Observable<PlayerTierResponse[]> {
        return this.http.get(`${SERVER_URL}/api/event/${eventNo}/all/players/`).pipe(map(results => results as PlayerTierResponse[]));
    }

    getTournamentPageUsersFantasyTeam(eventNo, userId): Observable<UserFantasyTeamResponse[]> {
        return this.http.get(`${SERVER_URL}/api/event/${eventNo}/tournament-player/user/${userId}`).pipe(
            map(results => results as UserFantasyTeamResponse[]));
    }

    getLeaderBoardUsersFantasyTeam(eventNo, userId): Observable<PlayerTierResponse[]> {
        return this.http.get(`${SERVER_URL}/api/event/${eventNo}/user/${userId}/team`).pipe(
            map(results => results as PlayerTierResponse[]));
    }

    removeAthleteFromTeam(data: UserFantasyTeamRequest) {
        console.log('Remove Athlete From Fantasy Team:', data);
        const options = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            }),
            data
        };

        return new Promise((resolve, reject) => {
            // tslint:disable-next-line:max-line-length
            this.http.delete(`${SERVER_URL}/api/fantasyTeam/delete/u/${data.userId}/p/${data.playerId}/s/${data.scheduleNo}/t/${data.tier}`, options).subscribe(
                s => {
                    resolve(s);
                },
                err => {
                    reject(err);
                }
            );
        }).catch(this.handleError);
    }

    addAthleteToTeam(data: UserFantasyTeamRequest) {
        console.log('Add Athlete to Fantasy Team:', data);

        return new Promise((resolve, reject) => {
            this.http
                .post(`${SERVER_URL}/api/fantasyTeam/add`, JSON.stringify(data), {
                    headers: new HttpHeaders({
                        Authorization: '',
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*'
                    })
                })
                .subscribe(
                    res => {
                        resolve(res);
                    },
                    err => {
                        reject(err);
                    }
                );
        }).catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.log(error);
        return Promise.reject(error.message || error);
    }
}
