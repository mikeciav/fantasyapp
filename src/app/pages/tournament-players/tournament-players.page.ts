import {TournamentResponse} from './../tournaments/tournament.model';
import {TournamentPlayerService} from './tournament-players.service';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router, NavigationExtras} from '@angular/router';
import {LoadingController, AlertController} from '@ionic/angular';
import {Storage} from '@ionic/storage';

import {PlayerTierResponse, UserFantasyTeamResponse, UserFantasyTeamRequest} from './player-tier-response';

@Component({
    selector: 'app-tournament-players',
    templateUrl: './tournament-players.page.html',
    styleUrls: ['./tournament-players.page.scss']
})
export class TournamentPlayersPage implements OnInit {
    event: TournamentResponse;
    tierSelected: any = 1;
    athleteTier: PlayerTierResponse[];
    athleteTierList: PlayerTierResponse[];
    userId: any = 0;
    tier1Count: any = 0;
    tier2Count: any = 0;
    tier3Count: any = 0;

    userTeam: UserFantasyTeamResponse[];
    loaded=false;
    descending = false;
    order: number = 1;
    column = 'seed';


    athleteFantasy: UserFantasyTeamRequest = new UserFantasyTeamRequest();
    slidesOpts = {
        slidesPerView: 6,
        freeMode: true,
        centeredSlides: false
    };
    isLoggedIn = true;

    constructor(
        private tournamentPlayerService: TournamentPlayerService,
        private route: ActivatedRoute,
        private router: Router,
        private loadingCtrl: LoadingController,
        private storage: Storage,
        private alertCtrl: AlertController
    ) {
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.event = this.router.getCurrentNavigation().extras.state.tournament;

                this.loadFantasyTeam(1);
            } else {
                this.router.navigate(['app/tournaments']);
            }
        });
    }

    ascDesc(){
        this.descending = !this.descending;
        this.order = this.descending ? 1 : -1;


    }
    sort(sortField:any){
        this.column =  sortField.target.value;
        this.descending = false;
        this.order = this.descending ? 1 : -1;


    }

    async showUseMustBeLoggedInToPlay() {
        const text = 'Please Login Or sign up to play';
        const alert = await this.alertCtrl.create({
            header: 'Login In or sign up!',
            message: text,
            buttons: ['Ok']
        });
        return await alert.present();
    }

    async howtoplay() {
        const text = 'How to Play';
        const alert = await this.alertCtrl.create({
            header:
                'To set your lineup, simply select your  players from the 3 tiers. You may choose 4 players for a total of 12 players on your roster.',
            message: text,
            buttons: ['Ok']
        });
        return await alert.present();
    }
    async toManyPlayers() {
        const text = 'Only 4 Players Per Tier';
        const alert = await this.alertCtrl.create({
            header: 'Only 4 Players Per Tier',
            message: 'You may choose 4 players for a total of 12 players on your roster.',
            buttons: ['Ok']
        });
        return await alert.present();
    }
    ngOnInit() {
        this.storage.get('userId').then(val => {
            if (val) {
                this.isLoggedIn = true;
            } else {
                this.isLoggedIn = false;
            }
        });
    }

    ionViewDidEnter() {
        this.userId = this.storage.get('userId').then(val => {
            if (val) {
                this.userId = val;
                this.loadFantasyTeam(this.tierSelected);
            } else {
                this.showUseMustBeLoggedInToPlay();
                this.storage.clear().then(v => {
                    this.router.navigate(['login']);
                });
            }
        });
    }

    async loadFantasyTeam(tier) {
        if (this.event) {
            const loading = await this.loadingCtrl.create({
                message: 'Getting Your Fantasy Team Please Wait ....'
            });
            await loading.present();

            this.userId = await this.storage.get('userId');
            this.tournamentPlayerService.getTournamentPageUsersFantasyTeam(this.event.scheduleNo, this.userId).subscribe(
                res => {

                    this.userTeam = res;
                    this.setTeamTierCount();
                    this.loadAthletes(tier);
                    loading.dismiss();
                },
                err => {
                    console.log('getuserfantasyerror', err);
                    loading.dismiss();
                }
            );
        }
    }

    onSelected(player: PlayerTierResponse) {

        if (this.userTeam.filter((value: UserFantasyTeamResponse) => value.tier === player.tier).length === 4) {
            this.toManyPlayers();
            return;
        }

        //check to see if partner is on the team already
        //use the name of the player and link it to the partners name on your team

        /*const partnerIndex = this.userTeam.findIndex(x => x.name === player.partnerName);

        if (partnerIndex > -1) {
            alert('Can not take player partner');
            return;
        }*/


        const index = this.userTeam.findIndex(x => x.playerId === player.playerId);
        if (index === -1) {

            this.addToTeam(player);
        } else {
            this.playerAlreadyExists();
            return;
        }


    }
    async playerAlreadyExists() {
        const text = 'Duplicate Player';
        const alert = await this.alertCtrl.create({
            header: 'Only 4 Players Per Tier',
            message: 'Player Already Exists On Your Team.',
            buttons: ['Ok']
        });
        return await alert.present();
    }
    onRemoved(player: PlayerTierResponse) {

        this.removeFromTeam(player);
    }

    async addToTeam(player: PlayerTierResponse) {
        if (this.event.status !== 2) {
            return;
        }
        const msg = 'Adding' + player.name + ' to your team! :)';
        const loading = await this.loadingCtrl.create({
            message: msg,
            spinner: 'circles',
            showBackdrop: true,
            duration: 60000
        });
        await loading.present();
        // check to see if you already took his or her partner for this tournament

        try {
            this.athleteFantasy = new UserFantasyTeamRequest();

            this.athleteFantasy.playerId = player.playerId;
            this.athleteFantasy.scheduleNo = this.event.scheduleNo;
            this.athleteFantasy.tier = player.tier;
            this.athleteFantasy.userId = this.userId;

            this.tournamentPlayerService
                .addAthleteToTeam(this.athleteFantasy)
                .then(res => {

                    const userFantasyTeamResponse = new UserFantasyTeamResponse();
                    userFantasyTeamResponse.imageUrl = player.imageUrl;
                    userFantasyTeamResponse.name = player.name;
                    userFantasyTeamResponse.playerId = player.playerId;
                    userFantasyTeamResponse.scheduleNo = this.event.scheduleNo;
                    userFantasyTeamResponse.tier = player.tier;
                    userFantasyTeamResponse.userId = this.userId;

                    this.userTeam.push(userFantasyTeamResponse);
                    this.setTeamTierCount();
                    this.loadFantasyTeam(player.tier);
                    loading.dismiss();
                })
                .catch(err => {
                    console.log('Error in Add Team', err);
                    loading.dismiss();
                    this.router.navigate(['app/tournaments']);
                });
        } catch (ex) {
            console.log('ERROR adding player', ex);
            loading.dismiss();
            this.router.navigate(['app/tournaments']);
        }


        // only add 4 players per tier.
        /* if (this.userTeam.filter((value: UserFantasyTeamResponse) => value.tier === player.tier).length === 4) {
           alert('Only 4 allowed');
           this.loadFantasyTeam(player.tier);
           loading.dismiss();
         } else {
           const index = this.userTeam.findIndex(x => x.playerId === player.playerId);
           // here you can check specific property for an object whether it exist in your array or not
           if (index === -1) {
             // add player to database

           } else {
             alert('Player Already Exists On Your Team');
             loading.dismiss();
           }
         }*/
    }

    async removeFromTeam(player: PlayerTierResponse) {
        if (this.event.status !== 2) {
            return;
        }
        const msg = 'Removing ' + player.name + ' from your team.';
        const loading = await this.loadingCtrl.create({
            message: msg,
            spinner: 'circles',
            showBackdrop: true,
            duration: 60000
        });
        await loading.present();
        try {
            this.athleteFantasy = new UserFantasyTeamRequest();
            this.athleteFantasy.playerId = player.playerId;
            this.athleteFantasy.scheduleNo = this.event.scheduleNo;
            this.athleteFantasy.tier = player.tier;
            this.athleteFantasy.userId = this.userId;

            this.tournamentPlayerService
                .removeAthleteFromTeam(this.athleteFantasy)
                .then(res => {
                    const removeUserFantasyTeamResponse = new UserFantasyTeamResponse();
                    removeUserFantasyTeamResponse.imageUrl = player.imageUrl;
                    removeUserFantasyTeamResponse.name = player.name;
                    removeUserFantasyTeamResponse.playerId = player.playerId;
                    removeUserFantasyTeamResponse.scheduleNo = this.event.scheduleNo;
                    removeUserFantasyTeamResponse.tier = player.tier;
                    removeUserFantasyTeamResponse.userId = this.userId;


                    const index = this.userTeam.indexOf(removeUserFantasyTeamResponse);
                    this.userTeam.splice(index, 1);
                    this.setTeamTierCount();
                    this.loadFantasyTeam(player.tier);
                    loading.dismiss();
                })
                .catch(err => {
                    console.log('Error in Add Team', err);
                    loading.dismiss();
                    this.router.navigate(['app/tournaments']);
                });
        } catch (ex) {
            console.log('TRY ERROR IN REMOVE', ex);
            loading.dismiss();
            this.router.navigate(['app/tournaments']);
        }
    }

    setTeamTierCount() {
        if (this.userTeam) {
            this.tier1Count = this.userTeam.filter((value: UserFantasyTeamResponse) => value.tier === 1).length;
            this.tier2Count = this.userTeam.filter((value: UserFantasyTeamResponse) => value.tier === 2).length;
            this.tier3Count = this.userTeam.filter((value: UserFantasyTeamResponse) => value.tier === 3).length;

        }
    }

    segmentChanged(event) {
        this.tierSelected = Number(event);
        if (this.event.status === 3) {
            this.loadAthletes(event);
        } else {
            this.athleteTier = this.athleteTierList.filter((value: PlayerTierResponse) => value.tier === Number(event));
        }
    }


    async loadAthletes(t) {
        let loading;
        loading = await this.loadingCtrl.create({
            message: 'Loading Athletes....',
            spinner: 'circles',
            showBackdrop: true,
            duration: 60000
        });
        try {
            if (this.event.status === 2 || this.event.status === 3) {
                loading = await this.loadingCtrl.create({
                    message: 'Loading Please Wait.',
                    spinner: 'circles',
                    showBackdrop: true,
                    duration: 90000
                });
            }
        } catch (err) {
        }

        await loading.present();
        try {
            this.tournamentPlayerService.getTournamentPlayersByEvent(this.event.scheduleNo).subscribe(
                res => {

                    this.athleteTierList = res;
                    this.athleteTier = this.athleteTierList.filter((value: PlayerTierResponse) => value.tier === t);
                    this.tierSelected = t;
                    loading.dismiss();
                    this.loaded = true;
                },
                err => {
                    console.log(err);
                    loading.dismiss();
                }
            );
        } catch (err) {
            loading.dismiss();
            this.router.navigate(['app/tournaments']);
        }
    }

    logOut() {
        this.storage.clear().then(v => {
            this.router.navigate(['login']);
        });
    }
}
