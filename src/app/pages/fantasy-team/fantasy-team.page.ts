import {AthleteViewModel} from './../athlete-tiers/athleteTierModel';
import {Component, OnInit} from '@angular/core';
import {Storage} from '@ionic/storage';
import {LoadingController, ModalController} from '@ionic/angular';
import {Router, ActivatedRoute, NavigationExtras} from '@angular/router';
import {AthleteTierService} from '../athlete-tiers/athlete-tiers.service';
import {TournamentModel, TournamentResponse} from '../tournaments/tournament.model';
import {TournamentService} from '../tournaments/tournament.service';
import {PlayerTierResponse} from '../tournament-players/player-tier-response';
import {TournamentPlayerService} from '../tournament-players/tournament-players.service';
import {AthleteProfileCardPage} from '../athlete-profile-card/athlete-profile-card.page';
import {LeaderBoardResponse} from '../leader-board/leader-board.model';
import {LeaderBoardService} from '../leader-board/leader-board.service';

@Component({
    selector: 'app-fantasy-team',
    templateUrl: './fantasy-team.page.html',
    styleUrls: ['./fantasy-team.page.scss']
})
export class FantasyTeamPage implements OnInit {
    tournament: TournamentModel;

    events: TournamentResponse[];
    event: TournamentResponse;
    userTeam: PlayerTierResponse[];
    userId = 0;
    scheduleNo = 1;
    dataReturned: any;
    leaderBoardResponse: LeaderBoardResponse;
    leaderBoardResponseList: LeaderBoardResponse[];
    order: number;
    column = 'grandPointsTotal';
    descending = false;
    loaded=false;
    constructor(
        private router: Router,
        private loadingCtrl: LoadingController,
        private route: ActivatedRoute,
        private storage: Storage,
        private service: TournamentService,
        private tournamentPlayerService: TournamentPlayerService,
        private modalController: ModalController,
        private leaderBoardService: LeaderBoardService,
    ) {
        // has to be in contructor
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.scheduleNo = this.router.getCurrentNavigation().extras.state.eventNo;
                this.userId = this.router.getCurrentNavigation().extras.state.user;
                this.leaderBoardResponse = this.router.getCurrentNavigation().extras.state.leaderResponse;
                this.event = this.router.getCurrentNavigation().extras.state.tournament;
                console.log('constructor:', this.scheduleNo);
                this.loadTeam(this.scheduleNo, this.userId);
            }else {
                this.router.navigate(['app/leaderboard']);
            }
        });

        this.loadTournametsData();
    }


    ngOnInit() {

    }
    ascDesc(){
        this.descending = !this.descending;
        this.order = this.descending ? 1 : -1;


    }
    sort(sortField:any){
        this.column =  sortField.target.value;
        this.descending = false;
        this.order = this.descending ? 1 : -1;


    }
    async openModal(player: PlayerTierResponse) {

        const modal = await this.modalController.create({
            component: AthleteProfileCardPage,
            componentProps: {
                paramID: player.playerId,
                paramName: player.name,
                paramImg: player.imageUrl,
                paramScheduleNo: this.scheduleNo,
                paramLocation: "location"
            }
        });

        modal.onDidDismiss().then(dataReturned => {
            if (dataReturned !== null) {
                //this.dataReturned = dataReturned.data;
            }
        });

        return await modal.present();
    }

    async loadTournametsData() {
        const loading = await this.loadingCtrl.create();
        await loading.present();
        this.service.getAVPEvents().subscribe(
            res => {
                this.events = res;
                loading.dismiss();
                this.getEvent(this.scheduleNo);
            },
            err => {
                console.log(err);
                loading.dismiss();
            }
        );
    }

    async loadTeam(eventNo, userId) {


        const loading = await this.loadingCtrl.create();
        await loading.present();
        this.tournamentPlayerService.getLeaderBoardUsersFantasyTeam(eventNo, userId).subscribe(
            val => {
                console.log(val);
                this.userTeam = val;
                this.userTeam.sort((a, b) => (a.grandPointsTotal > b.grandPointsTotal ? -1 : 1));
                this.loaded = true;
                loading.dismiss();
            },
            err => {
                console.log(err);
                loading.dismiss();
            }
        );
    }

    tournamentChanged(eventNo: any) {


        // Call our service function which returns an Observable
        this.scheduleNo = eventNo.target.value;

        this.getLeaderBoardResponse(this.scheduleNo, this.userId);
        this.loadTeam(this.scheduleNo, this.userId);
        this.getEvent(this.scheduleNo);
    }

    getEvent(scheduleNo){
        this.event = new TournamentResponse();
        this.events.forEach((ev) => {

            if (this.scheduleNo == ev.scheduleNo) {
                this.event = ev;

            }
        });
    }

    getLeaderBoardResponse(scheduleNo, userId) {
        this.leaderBoardService.getLeaderBoardByScheduleNo(scheduleNo).subscribe(
            res => {

                res.forEach((val: LeaderBoardResponse) => {
                    if (userId == val.userId) {
                        this.leaderBoardResponse = val;

                    }

                });
            },
            err => {
                console.log(err);

            }
        );
    }

    async doRefresh(event) {
        setTimeout(() => {
            this.loadTeam(this.scheduleNo, this.userId);
            event.target.complete();
        }, 2000);
    }

    logOut() {
        this.storage.clear().then(v => {
            this.router.navigate(['login']);
        });
    }


}
